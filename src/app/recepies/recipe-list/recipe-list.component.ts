import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
 @Output() recipeWasSelected = new EventEmitter<Recipe>();
  recipes: Recipe[] = [
    new Recipe("A Test Recipe", "This is test mushroom",
      "https://www.wellplated.com/wp-content/uploads/2017/03/Crock-Pot-Mexican-Casserole.jpg"),
    new Recipe("A Test Recipe of Mushroom", "This is test description",
      "http://4.bp.blogspot.com/-uWHNNUgBYfs/UXICgJwKNjI/AAAAAAAALnM/GcVU_6eZGhI/s1600/4.jpg")
  ];

  constructor() { }

  ngOnInit() {
  }

  onRecipeSelected(recipe : Recipe) {
    this.recipeWasSelected.emit(recipe);

  }

}
